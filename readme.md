# CANGKEMAN

<p> Cangkeman adalah media kecil tempat penampungan misuh-misuh, tempat rehat sejenak dari bisingnya arus informasi digital dan kesibukan dunia nyata. Dibangun atas dasar rasa jengah dan keresahan terhadap hal-hal yang terjadi di sekitar. Kami pun mencoba menjadi wadah bagi penulis yang memiliki keresahan dan kejengahan yang beragam. Konten yang kritis, analitik, serta menggelitik adalah asupan-asupan bergizi bagi kami dan pembaca setia Cangkeman </p>

<p> www.cangkeman.net </p>